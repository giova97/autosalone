//
//  IndiceFoto.swift
//  autosalone
//
//  Created by Paolo Berluti on 21/08/17.
//  Copyright © 2017 Giovanni Giometti. All rights reserved.
//

import Foundation
import UIKit

class IndiceFoto {
    var placeholder: Bool
    var foto: UIImage?
    var indice:Int
    
    init(placeholder:Bool, indice:Int) {
        self.placeholder = placeholder
        self.indice = indice
        if placeholder
        {
            self.foto = #imageLiteral(resourceName: "imagePlace")
        }
    }
    
    init(placeholder:Bool, foto:UIImage, indice:Int) {
        self.placeholder = placeholder
        self.indice = indice
        self.foto = foto
    }
    
    
    func setplaceholder()
    {
        self.placeholder = true
        self.foto = #imageLiteral(resourceName: "imagePlace")
    }
    
    
    
}

