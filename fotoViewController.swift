//
//  fotoViewController.swift
//  autosalone
//
//  Created by Paolo Berluti on 11/08/17.
//  Copyright © 2017 Giovanni Giometti. All rights reserved.
//

import UIKit
protocol fotoViewControllerDelegate {
    func deleteFoto(index: Int)
}
class fotoViewController: UIViewController {
    
    var indexFoto: Int?
    
    var imageToShow: UIImage?

    @IBOutlet weak var fotoView: UIView!
    
    @IBOutlet weak var image: UIImageView!
    
    var delegate: fotoViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.image.image = imageToShow
        self.fotoView.layer.cornerRadius = 10
        self.fotoView.layer.masksToBounds = true
        self.image.layer.cornerRadius = 10
        self.image.layer.masksToBounds = true
        
        
        // Do any additional setup after loading the view.
    }

    
    @IBAction func cancel(_ sender: Any) {
        self.delegate?.deleteFoto(index: indexFoto!)
        self.dismiss(animated: true, completion: nil)
        
    }

    @IBAction func Ok(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
