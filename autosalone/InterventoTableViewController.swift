//
//  InterventoTableViewController.swift
//  autosalone
//
//  Created by Giovanni Giometti on 11/07/17.
//  Copyright © 2017 Giovanni Giometti. All rights reserved.
//

import UIKit
import MessageUI

class InterventoTableViewController: BaseTableViewController, UIPickerViewDelegate,UIPickerViewDataSource, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, fotoViewControllerDelegate, MFMailComposeViewControllerDelegate, UITextViewDelegate{
    
    //array numero celle istanziate
    var celle = ["data","servizio", "km","note","cellulare","nome", "documento", "invia"]
    
    //i dati storati della view
    var data = ["data":"","ora":"","servizio":"","km":"","note":"","cellulare":"","email":"","nome":"","cognome":""]
    
    //Array ore
    let ore = [["9","10","11","12","13","15","16","17"],["00", "15", "30", "45"]]
    
    //Array intervento
    let intervento = ["tagliando completo", "cambio gomme", "cambio olio", "revisione MCTC", "carrozzeria", "sostituzione batteria", "sostituzione freni", "lavaggio completo"]
    
    
    //creazione dei piker generici
    var orePickerView = UIPickerView()
    var interventoPickerView = UIPickerView()
    
    //Creazione del dataPiker e del tolBar
    var pikerDate = UIDatePicker()
    
    let toolBarForDate = UIToolbar().toolbarPiker(mySelect: #selector(donePressedDate))
    let toolBarForTime = UIToolbar().toolbarPiker(mySelect: #selector(donePressedTime))
    let toolBarForIntervento = UIToolbar().toolbarPiker(mySelect: #selector(donePressedIntervento))
    
    //variabile per testare se la cella dei km è espansa
    var isExpandCell = false
    var foto = [UIImage]()
    
    
    var indiceFoto: [IndiceFoto] = []
    var imagePicker: UIImagePickerController!
    //serve per capire quale immagine è stata tappata sulla cella dei documenti
    var selectedImage = 0
    var placeholder = true
    @IBOutlet var myTable: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        orePickerView.delegate = self
        orePickerView.tag = 0
        
        self.pikerDate.datePickerMode = .date
        self.pikerDate.tag = 0
        
        interventoPickerView.delegate = self
        interventoPickerView.tag = 1
        
        var foto1 = IndiceFoto(placeholder: true, indice: 1)
        var foto2 = IndiceFoto(placeholder: true, indice: 2)
        self.indiceFoto.append(foto1)
        self.indiceFoto.append(foto2)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return celle.count
    }
    
    
    //MARK: CELL_FOR_ROW
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(self.celle[indexPath.row])
        let cell = tableView.dequeueReusableCell(withIdentifier: self.celle[indexPath.row], for: indexPath) as! interventoCell
        let identifier = String(describing: cell.reuseIdentifier!)
        var dataForCell = self.data[identifier]
        
        switch identifier {
        case "data":
            //Date
            cell.dateTextField.inputView = self.pikerDate
            cell.dateTextField.inputAccessoryView = toolBarForDate
            cell.dateTextField.delegate = self
            //Orario
            cell.oraTextField.inputView = self.orePickerView
            cell.oraTextField.inputAccessoryView = toolBarForTime
            cell.setupCell(data: self.data, identifier: cell.reuseIdentifier!)
            let color = UIColor.red
            if cell.dateTextField.text != ""{cell.animate(label: cell.giornoEtichetta, fadeIn: true)}else{cell.animate(label: cell.giornoEtichetta, fadeIn: false)}
            if cell.oraTextField.text != ""{cell.animate(label: cell.oraEtichetta, fadeIn: true)}else{cell.animate(label: cell.oraEtichetta, fadeIn: false)}
        case "servizio":
            //Intervento
            cell.interventoTextField.inputView = self.interventoPickerView
            cell.interventoTextField.inputAccessoryView = toolBarForIntervento
            cell.setupCell(data: self.data, identifier: cell.reuseIdentifier!)
            if cell.interventoTextField.text != ""{cell.animate(label: cell.interventoEtichetta, fadeIn: true)}else{cell.animate(label: cell.interventoEtichetta, fadeIn: false)}
        case "km":
            cell.kmTextField.delegate = self
            cell.kmTextField.tag = indexPath.row
            cell.kmTextField.addTarget(self, action: #selector(textFiledDidChange), for: .editingChanged)
            cell.setupCell(data: self.data, identifier: cell.reuseIdentifier!)
        case "note":
            //NOTE
            cell.setupCell(data: data, identifier: cell.reuseIdentifier!)
            if data["note"] == ""{
                cell.placehorderNote.isHidden = false
            }else
            {
                cell.placehorderNote.isHidden = true
            }
            //            cell.noteTextView.inputAccessoryView = toolBarForMarca
            cell.noteTextView.delegate = self
        case "cellulare":
            cell.cellulareTextField.delegate = self
            cell.cellulareTextField.tag = indexPath.row
            cell.cellulareTextField.addTarget(self, action: #selector(textFiledDidChange), for: .editingChanged)
            
            cell.emailTextField.delegate = self
            cell.emailTextField.tag = 20
            cell.emailTextField.addTarget(self, action: #selector(textFiledDidChange), for: .editingChanged)
            cell.setupCell(data: data, identifier: identifier)
        case "nome":
            cell.nomeTextField.delegate = self
            cell.nomeTextField.tag = indexPath.row
            cell.nomeTextField.addTarget(self, action: #selector(textFiledDidChange), for: .editingChanged)
            
            cell.cognomeTextField.delegate = self
            cell.cognomeTextField.tag = 21
            cell.cognomeTextField.addTarget(self, action: #selector(textFiledDidChange), for: .editingChanged)
            cell.setupCell(data: data, identifier: identifier)
        case "documento":
            for i in self.indiceFoto
            {
                cell.setImage(value: i)
            }
            
        case "invia":
            print("INVIA")
        default:
            break
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at:indexPath, animated: false)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let identifier = self.celle[indexPath.row]
        
        switch identifier {
        case "data":
            return 86 //ora
        case "servizio":
            return 86 //servizio
        case "km":
            if self.isExpandCell{
                return 86
            }
            return 0 //km
        case "note":
            return 120
        case "cellulare":
            return 86 //cell-email
        case "nome":
            return 86 //nome-cognome
        case "documento":
            return 160 //Foto
        case "invia":
            return 80 //invia
        default:
            break
        }
        return 0
    }
    
    // MARK: PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if pickerView.tag == 0{
            return ore.count
        }
        return 1
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return ore[component].count
        case 1:
            return intervento.count
        default:
            break
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return ore[component][row]
        case 1:
            return intervento[row]
        default:
            break
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 0:
            let ora = ore[0][pickerView.selectedRow(inComponent: 0)]
            let minuti = ore[1][pickerView.selectedRow(inComponent: 1)]
            data["ora"] = ora + ":" + minuti
            pickerView.endEditing(false)
        case 1:
            let intervento = self.intervento[pickerView.selectedRow(inComponent: 0)]
            data["servizio"] = intervento
            pickerView.endEditing(false)
        default:
            break
        }
    }
    
    //MARK: TextField Delegate
    var kmCharatterCount = 0
    
    func textFiledDidChange(textField: UITextField){
        let colorGray = UIColor.gray
        textField.attributedPlaceholder = NSAttributedString(string: textField.placeholder!, attributes: [NSForegroundColorAttributeName : colorGray])
        var identifier:String
        
        if textField.tag == 20
        {
            identifier = "email"
        }else if textField.tag == 21{
            identifier = "cognome"
        }else{
            identifier = self.celle[textField.tag]
        }
        if textField.restorationIdentifier == "km"
        {
            if (textField.text?.characters.count)! > kmCharatterCount
            {
                self.data[identifier] = textField.text
                if let range = self.data[identifier]?.range(of: " Km") {
                    self.data[identifier]?.removeSubrange(range)
                }
                textField.text = ""
                textField.text = data["km"]! + " Km"
            }else{
                if (textField.text?.characters.count)! < 4
                {
                    data["km"] = ""
                    textField.text = ""
                    textField.endEditing(true)
                }else{
                    var name: String = data["km"]!
                    let endIndex = name.index(name.endIndex, offsetBy: -1)
                    let truncated = name.substring(to: endIndex)
                    data["km"] = truncated
                    textField.text = data["km"]! + " Km"
                }
                
            }
            kmCharatterCount = (textField.text?.characters.count)!
        }else{
            self.data[identifier] = textField.text
        }
        animatingLabel(identifier: identifier)
    }
    //MARK: TextView Delegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        let index = IndexPath(item: 3, section: 0)
        let cell = myTable.cellForRow(at: index) as! interventoCell
        cell.placehorderNote.textColor = UIColor.lightGray
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        data["note"] = textView.text
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let count = textView.text.characters.count
        let index = IndexPath(item: 3, section: 0)
        let cell = myTable.cellForRow(at: index) as! interventoCell
        if count > 0 {
            cell.placehorderNote.isHidden = true
            cell.animateLabel(cell: cell, identifier: "note", fadeIn: true)
        }else{
            cell.placehorderNote.isHidden = false
            cell.animateLabel(cell: cell, identifier: "note", fadeIn: false)
        }
    }
    
    
    
    
    @IBAction func menuAction(_ sender: Any) {
        self.menuAction()
    }
    
    //MARK: PRENOTA INTERVENTO
    @IBAction func pressPrenotaButton(_ sender: Any) {
        var ok = false
        let servizio:String = data["servizio"]! as String
        

        if data["data"] != "" && data["ora"] != "" && data["servizio"] != "" && data["cellulare"] != "" && data["email"] != "" && data["nome"] != "" && data["cognome"] != ""{
            
            switch servizio {
            case "tagliando completo", "cambio olio", "sostituzione freni":
                if indiceFoto.count > 0 || (data["km"] != "")
                {
                    if indiceFoto[0].placeholder == false || indiceFoto[1].placeholder == false{
                        ok = true
                        formattaEmail(servizio: servizio)
                    }
                }
            default:
                break
            }
            
        }
        if !ok{
            let alert = UIAlertView(title: "Attenzione", message: "Non tutti i campi obbligatori sono stati riempiti", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            let cells = self.tableView.visibleCells as! Array<interventoCell>
            
            for cell in cells {
                cell.checkTextfield(cell: cell, servizio: servizio)
            }
            
        }
    }
    
    var finalString: String?
    func formattaEmail(servizio: String)
    {
        switch servizio {
        case "tagliando completo", "cambio olio", "sostituzione freni":
            self.finalString = "Salve,\ninvio la presente per richiedere la prenotazione per un intervento all'auto vettura. Di seguito i dati necessari per la prenotazione:\nGiorno: \(data["data"]!) ora: \(data["ora"]!)\nIntervento: \(data["servizio"]!)\nKm: \(data["km"]!)\nNome: \(data["nome"]!)\nCognome: \(data["cognome"]!)\nCellulare:\(data["cellulare"]!)\nEmail: \(data["email"]!)\n Di seguito allego foto del libretto di circolazione.\n\nP.s. \(data["note"]!)"
        default:
            self.finalString = "Salve,\ninvio la presente per richiedere la prenotazione per un intervento all'auto vettura. Di seguito i dati necessari per la prenotazione:\nGiorno: \(data["data"]!) ora: \(data["ora"]!)\nIntervento: \(data["servizio"]!)\nKm: \(data["km"]!)\nNome: \(data["nome"]!)\nCognome: \(data["cognome"]!)\nCellulare:\(data["cellulare"]!)\nEmail: \(data["email"]!)\n Di seguito allego foto del libretto di circolazione.\n\n"
        }
        if( MFMailComposeViewController.canSendMail() ) {
            print("Can send email.")
            
            let mailComposer = configureMailComposer()
            
            //Set the subject and message of the email
            
            self.present(mailComposer, animated: true, completion: nil)
        }
    }
    
    func configureMailComposer()->MFMailComposeViewController
    {
        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self
        mailComposer.setToRecipients(["paolo.berluti@gmail.com"])
        mailComposer.setSubject("Preventivo \(data["nome"]!) \(data["cognome"]!)")
        mailComposer.setMessageBody(finalString!, isHTML: false)
        for image in indiceFoto
        {
            if !image.placeholder{
                let imageData: NSData = (UIImageJPEGRepresentation(image.foto!, 0.5) as? NSData)!
                mailComposer.addAttachmentData(imageData as Data, mimeType: "image/jpeg", fileName: "image.jpg")
            }
        }
        return mailComposer
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        print(result)
        self.dismiss(animated: true, completion: nil)
    }
    
    func test() {
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.mailgun.net/v3/sandboxde2372a8c7d343d19991b83d0f91ba93.mailgun.org/messages")! as URL)
        request.httpMethod = "POST"
        let data = "from: Excited User <paolo.berluti@gmail.com>&to: [paolo.berluti@gmail.com]&subject:Hello&text:Testinggsome Mailgun awesomness!"
        request.httpBody = data.data(using: String.Encoding.ascii)
        request.setValue("key-ab2e87eccdbca060aec372a36b71b122", forHTTPHeaderField: "api")
        let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
            
            if let error = error {
                print(error)
            }
            if let response = response {
                print("url = \(response.url!)")
                print("response = \(response)")
                let httpResponse = response as! HTTPURLResponse
                print("response code = \(httpResponse.statusCode)")
            }
            
            
        })
        task.resume()
    }
    
    func donePressedDate(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        self.data["data"] = dateFormatter.string(from: pikerDate.date)
        self.myTable.reloadData()
    }
    
    func donePressedTime(){
        if data["ora"] == ""{
            let ora = ore[0][0]
            let minuti = ore[1][0]
            data["ora"] = ora + ":" + minuti
        }
        self.myTable.reloadData()
    }
    
    func donePressedIntervento(){
        if var servizio = data["servizio"] {
            if servizio == ""
            {
                servizio = self.intervento[0]
            }
            
            if servizio == "tagliando completo" || servizio == "cambio olio" || servizio == "sostituzione freni"{
                self.isExpandCell = true
            }else{
                self.isExpandCell = false
            }
            self.data["servizio"] = servizio
        }
        self.myTable.reloadData()
    }
    
    @IBAction func openPhoto(_ sender: UIButton) {
        //        self.btnEdit.setTitleColor(UIColor.white, for: .normal)
        //        self.btnEdit.isUserInteractionEnabled = true
        //
        self.selectedImage = sender.tag
        let alert = UIAlertController(title: "Scegli Immagine", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Fotocamera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Galleria Foto", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancella", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera(){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    
    func openGallery(){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        //        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        if var index = indiceFoto.index(where: {$0.indice == selectedImage}) {
            self.indiceFoto[index].foto = image
            self.indiceFoto[index].placeholder = false
        }
        //        foto.append(image)
        //        self.placeholder = false
        self.tableView.reloadData()
        dismiss(animated:true, completion: nil)
    }
    
    @IBAction func visualizza(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "fotoView") as! fotoViewController
        vc.delegate = self
        self.selectedImage = sender.tag
        if var index = indiceFoto.index(where: {$0.indice == self.selectedImage}) {
            vc.imageToShow = self.indiceFoto[index].foto
            vc.indexFoto = self.indiceFoto[index].indice
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func deleteFoto(index: Int) {
        if var index = indiceFoto.index(where: {$0.indice == selectedImage}) {
            self.indiceFoto[index].setplaceholder()
        }
        self.tableView.reloadData()
    }
    
    func animatingLabel( identifier:String)
    {
        var identifierCell = identifier
        if identifier == "ora"{identifierCell = "data"}
        if identifier == "cognome"{identifierCell = "nome"}
        if identifier == "email"{identifierCell = "cellulare"}
        if data[identifier] != ""
        {
            let cells = self.tableView.visibleCells as! Array<interventoCell>
            for cell in cells{
                if cell.reuseIdentifier == identifierCell{
                    cell.animateLabel(cell: cell, identifier: identifier, fadeIn: true)
                }
            }
            
        }else{
            let cells = self.tableView.visibleCells as! Array<interventoCell>
            for cell in cells{
                if cell.reuseIdentifier == identifierCell{
                    cell.animateLabel(cell: cell, identifier: identifier, fadeIn: false)
                }
            }
        }
    }
}
// Estensione per creare il Toolbar
extension UIToolbar {
    
    func toolbarPiker(mySelect : Selector) -> UIToolbar {
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: mySelect)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([ spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
}

