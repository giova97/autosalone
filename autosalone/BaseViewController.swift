//
//  BaseViewController.swift
//  autosalone
//
//  Created by Paolo Berluti on 11/07/17.
//  Copyright © 2017 Giovanni Giometti. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    var menu_vc: MenuViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menu_vc = self.storyboard?.instantiateViewController(withIdentifier: "menuViewController") as! MenuViewController
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        
        self.view.addGestureRecognizer(swipeRight)
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    
    func respondToGesture(gesture: UISwipeGestureRecognizer)
    {
        switch gesture.direction {
        case UISwipeGestureRecognizerDirection.right:
            self.opneMenu()
        case UISwipeGestureRecognizerDirection.left :
            self.closeMenu()
            
        default:
            break
        }
    }
  
    func  menuAction() {
        if AppDelegate.menu_bool{
            self.opneMenu()
        }
        else{
            self.closeMenu()
        }
    }
    
    func opneMenu()
    {
        self.menu_vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        self.addChildViewController(self.menu_vc)
        self.view.addSubview(self.menu_vc.view)
        
        AppDelegate.menu_bool = false
    }
    
    
    func closeMenu()
    {
        self.menu_vc.animationOut() { success in
            self.menu_vc.view.removeFromSuperview()
            AppDelegate.menu_bool = true
        }        
    }

}
extension UIViewController {
    
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.25
        self.traitCollection.containsTraits(in: UITraitCollection.init())
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        present(viewControllerToPresent, animated: false)
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false)
    }
}
