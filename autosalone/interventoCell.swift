//
//  interventoCell.swift
//  autosalone
//
//  Created by Giovanni Giometti on 12/07/17.
//  Copyright © 2017 Giovanni Giometti. All rights reserved.
//

import UIKit

class interventoCell: UITableViewCell{
    
    @IBOutlet weak var dateTextField: UITextField!
    
    @IBOutlet weak var oraTextField: UITextField!
    
    @IBOutlet weak var interventoTextField: UITextField!
    
    @IBOutlet weak var noteTextField: UITextField!
    
    @IBOutlet weak var cellulareTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var nomeTextField: UITextField!
    
    @IBOutlet weak var cognomeTextField: UITextField!
    
    @IBOutlet weak var kmTextField: UITextField!
    
    @IBOutlet weak var prenotaButton: UIButton!
    
    @IBOutlet weak var img1: UIImageView!
    
    @IBOutlet weak var img2: UIImageView!
    
    @IBOutlet weak var btnImg1: UIButton!
    
    @IBOutlet weak var btnImg2: UIButton!
    
    @IBOutlet weak var visualizzaImg1: UIButton!
    
    @IBOutlet weak var visualizzaBtn2: UIButton!
    
    @IBOutlet weak var viewImg2: UIView!
    
    @IBOutlet weak var noteTextView: UITextView!
    
    @IBOutlet weak var placehorderNote: UILabel!
    
    //Etichette
    @IBOutlet weak var giornoEtichetta: UILabel!
    @IBOutlet weak var oraEtichetta: UILabel!
    @IBOutlet weak var interventoEtichetta: UILabel!
    @IBOutlet weak var kmEtichetta: UILabel!
    @IBOutlet weak var noteEtichetta: UILabel!
    @IBOutlet weak var cellulareEtichetta: UILabel!
    @IBOutlet weak var emailEtichetta: UILabel!
    @IBOutlet weak var cognomeEtichetta: UILabel!
    @IBOutlet weak var nomeEtichetta: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupCell(data: [String :String], identifier: String){
        switch identifier {
        case "data":
            if let giorno = data["data"]{
                if check(value: giorno){
                    self.dateTextField.text = (data["data"])! as String
                }
            }
            
            if let ora = data["ora"]{
                if check(value: ora){
                    self.oraTextField.text = (data["ora"])! as String
                }
            }
        case "servizio":
            if let intervento = data["servizio"]{
                if check(value: intervento){
                    self.interventoTextField.text = (data["servizio"])! as String
                }
            }
        case "km":
            self.kmTextField.addDoneButtonOnKeyboard()
        case "note":
            self.noteTextView.addDoneButtonOnKeyboard()
        case "cellulare":
            self.cellulareTextField.addDoneButtonOnKeyboard()
            self.emailTextField.addDoneButtonOnKeyboard()
        case "nome":
            self.nomeTextField.addDoneButtonOnKeyboard()
            self.cognomeTextField.addDoneButtonOnKeyboard()

        default:
            break
        }
    }
    func setImage(value: IndiceFoto)
    {
        if value.placeholder
        {
            switch value.indice {
            case 1:
                self.img1.image = value.foto
                self.btnImg1.alpha = 1
                self.visualizzaImg1.alpha = 0
            case 2:
                self.img2.image = value.foto
                self.btnImg2.alpha = 1
                self.visualizzaBtn2.alpha = 0
            default:
                self.img1.image = value.foto
            }
        }else{
            switch value.indice {
            case 1:
                self.img1.image = value.foto
                self.btnImg1.alpha = 0
                self.visualizzaImg1.alpha = 1
            case 2:
                self.img2.image = value.foto
                self.btnImg2.alpha = 0
                self.visualizzaBtn2.alpha = 1
            default:
                self.img1.image = value.foto
            }
        }
    }
    func check(value: String) -> Bool
    {
        if value != ""
        {
            return true
        }
        return false
    }
    
    func checkTextfield(cell: interventoCell, servizio: String){
        let colorRed = UIColor.red
        for child in cell.contentView.subviews {
            
            if let textfield = child as? UITextField {
                if textfield.text == ""
                {
                    if textfield.restorationIdentifier == "note"{
                        
                    } else {
                        textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder!, attributes: [NSForegroundColorAttributeName : colorRed])
                    }
                }
            } else if let label = child as? UILabel
            {
                if label.restorationIdentifier == "placeNote"{
                    if (servizio == "Carrozzeria" || servizio == "Meccanica"){
                        label.textColor = colorRed
                    }else{
                        label.textColor = UIColor.lightGray
                    }
                }
            }
        }
    }
    
    func animateLabel(cell: interventoCell, identifier: String, fadeIn:Bool)
    {
        var identifier = identifier + "Etc"
        for child in cell.contentView.subviews {
            if let label = child as? UILabel {
                if label.restorationIdentifier == identifier{
                    self.animate(label: label, fadeIn: fadeIn)
                }
            }
        }
    }
    //MARK: ANIMATION
    func animate(label: UILabel, fadeIn:Bool) {
        var origin = label.center.y
        if fadeIn{
            if label.alpha == 0 {
                label.center.y = origin + 20
                UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: ({
                    label.center.y = origin//self.view.frame.height - 20//0.3 /// 1.008
                    label.alpha = 1
                }), completion: nil)
            }
        } else {
            if label.alpha == 1 {
                //                label.center.y = self.view.frame.height + 30
                UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: ({
                    label.center.y = origin + 20//label.center.y + 20//self.view.frame.height + 0.3 /// 1.008
                    label.alpha = 0
                }), completion: { (finished) -> Void in
                    label.center.y = origin})
            }
        }
    }

}
