//
//  ContattiViewController.swift
//  autosalone
//
//  Created by Giovanni Giometti on 17/07/17.
//  Copyright © 2017 Giovanni Giometti. All rights reserved.
//

import UIKit
import MapKit

class ContattiViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var myMapView: MKMapView! //dichiarazione outlet mappa 
    
    var managerPosizione: CLLocationManager! //gestisce un oggetto LocationManager
    var posizioneUtente: CLLocationCoordinate2D! //conserva le tue coordinate (LocationCoordinate):

}
