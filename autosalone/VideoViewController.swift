//
//  VideoViewController.swift
//  autosalone
//
//  Created by Paolo Berluti on 17/07/17.
//  Copyright © 2017 Giovanni Giometti. All rights reserved.
//

import UIKit
import MediaPlayer

class VideoViewController: UIViewController {
    
    var player: AVPlayer?
    var playerLayer:AVPlayerLayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let videoURL = URL(fileURLWithPath:Bundle.main.path(forResource: "video", ofType: "mov")!)
        let item = AVPlayerItem(url: videoURL)
        NotificationCenter.default.addObserver(self, selector: #selector(VideoViewController.playerDidFinishPlaying(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: item)
        
        player = AVPlayer(playerItem: item)
        player?.currentItem?.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions(), context: nil)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.frame = self.view.bounds
        //        playerLayer.backgroundColor = UIColor(red: 12/255.0, green: 70/255.0, blue: 34/255.0, alpha: 1).cgColor
        self.view.layer.addSublayer(playerLayer!)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if player?.currentItem?.status == AVPlayerItemStatus.readyToPlay {
            print("videoIsReady")
            player?.play()
        }
    }
    
    
    func playerDidFinishPlaying(_ note: Notification) {
        print("playerDidFinishPlaying")
        playerLayer?.removeFromSuperlayer()
        self.showHome()
    }
    
    func showHome()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if AppDelegate.storeViewController == "Home"{
            let controller = storyboard.instantiateViewController(withIdentifier: "homeViewController") as! UINavigationController
            self.presentDetail(controller)
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
