//
//  PreventivoTableViewController.swift
//  autosalone
//
//  Created by Giovanni Giometti on 18/07/17.
//  Copyright © 2017 Giovanni Giometti. All rights reserved.
//

import UIKit
import MessageUI

class PreventivoTableViewController: BaseTableViewController, UIPickerViewDelegate, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, fotoViewControllerDelegate, MFMailComposeViewControllerDelegate, UITextViewDelegate{
    
    //array numero celle istanziate
    var celle = ["marca","modello","servizio", "km","note","cellulare","nome", "documento", "invia"]
    
    //i dati storati della view
    var data = ["marca":"","modello":"","km":"","servizio":"","note":"","cellulare":"","email":"","nome":"","cognome":""]
    
    //Array marca
    let marca = ["Abarth","Acura","Alfa Romeo","Aston Martin","Audi","Bentley","BMW","Bugatti","Cadillac","Chevrolet","Citroen","Corvette","Dacia","Daewoo","Daihatsu","Dodge","Ferrari","Fiat","Ford","Honda","HUMMER","Hyundai","Infiniti","Isuzu","Iveco","Jaguar","Jeep","Kia","Lada","Lamborghini","Lancia","Land Rover","Lexus","Lotus","Mahindra","Maserati","Maybach","Mazda","McLaren","Mercedes-Benz","Mini","Mitsubishi","Nissan","Opel","Pagani","Peugeot","Piaggio","Porsche","Reanault","Rolls-Royce","Rover","Saab","Santana","Seat","Scoda","Smart","SsangYong","Subaru","Suzuky","Tata","Tesla","Toyota","Volkswagen","Volvo","Altro"]
    
    let infoCarrozzeria = "Inserisci la foto del libretto e del danno alla carrozzeria."
    let infoMeccanica   = "Inserisci la foto del libretto."
    let infoGomme       = "Inserisci la foto del libretto."
    //Array servizio
    let servizio = ["Carrozzeria","Meccanica","Gomme"]
    
    //creazione dei piker generici
    var marcaPickerView = UIPickerView()
    var servizioPickerView = UIPickerView()
    

    
    //creazione toolBar
    let toolBarForMarca = UIToolbar().ToolbarPiker(mySelect: #selector(donePressedMarca))
    let toolBarForServizio = UIToolbar().ToolbarPiker(mySelect: #selector(donePressedServizio))
    let toolBarForNote = UIToolbar().ToolbarPiker(mySelect: #selector(donePressedMarca))
    //variabile per testare se la cella dei servizi è espansa
    var isExpandCell = false
    var indiceFoto = [IndiceFoto]()
    var imagePicker: UIImagePickerController!
    //serve per capire quale immagine è stata tappata sulla cella dei documenti
    var selectedImage = 0
    var placeholder = true
    
    @IBOutlet var preventivoTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        marcaPickerView.delegate = self
        marcaPickerView.tag = 0
        
        servizioPickerView.delegate = self
        servizioPickerView.tag = 2
        
        var foto1 = IndiceFoto(placeholder: true, indice: 1)
        var foto2 = IndiceFoto(placeholder: true, indice: 2)
        self.indiceFoto.append(foto1)
        self.indiceFoto.append(foto2)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return celle.count
    }
    
    
    //MARK: CELL_FOR_ROW
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print(self.celle[indexPath.row])
        let cell = tableView.dequeueReusableCell(withIdentifier: self.celle[indexPath.row], for: indexPath) as! preventivoCell
        let identifier = String(describing: cell.reuseIdentifier!)
        var dataForCell = self.data[identifier]
        switch identifier {
            
        case "marca":
            //Marca
            cell.marcaTextField.inputView = self.marcaPickerView
            cell.marcaTextField.inputAccessoryView = toolBarForMarca
            cell.setupCell(data: dataForCell!, identifier: cell.reuseIdentifier!)
            if cell.marcaTextField.text != ""{cell.animate(label: cell.marcaEtichetta, fadeIn: true)}else{cell.animate(label: cell.marcaEtichetta, fadeIn: false)}
        case "modello":
            //Modello
            cell.modelloTextField.delegate = self
            cell.modelloTextField.tag = indexPath.row
            cell.modelloTextField.addTarget(self, action: #selector(textFiledDidChange), for: .editingChanged)
            cell.setupCell(data: dataForCell!, identifier: cell.reuseIdentifier!)
            if cell.modelloTextField.text != ""
            {
                cell.animate(label: cell.modelloEtichetta, fadeIn: true)
            }else{
                cell.animate(label: cell.modelloEtichetta, fadeIn: false)
            }
        case "km":
            cell.kmTextField.delegate = self
            cell.kmTextField.tag = indexPath.row
            cell.kmTextField.addTarget(self, action: #selector(textFiledDidChange), for: .editingChanged)
            cell.setupCell(data: dataForCell!, identifier: cell.reuseIdentifier!)
        case "servizio":
            cell.servizioTextField.inputView = self.servizioPickerView
            cell.servizioTextField.inputAccessoryView = toolBarForServizio
            cell.servizioTextField.allowsEditingTextAttributes = false
            cell.servizioTextField.delegate = self
            if dataForCell == "Meccanica" || dataForCell == "Gomme"
            {
                self.isExpandCell = true
            }
            else
            {
                self.isExpandCell = false
            }
            if data["servizio"] != ""{cell.animate(label: cell.servizioEtichetta, fadeIn: true)}else{cell.animate(label: cell.servizioEtichetta, fadeIn: false)}
            cell.setupCell(data: dataForCell!, identifier: cell.reuseIdentifier!)
        case "note":
            print("")
            cell.noteTextField.delegate = self
            cell.noteTextField.tag = indexPath.row
            cell.noteTextField.addTarget(self, action: #selector(textFiledDidChange), for: .editingChanged)
            cell.setupCell(data: dataForCell!, identifier: cell.reuseIdentifier!)
            if data["note"] == ""{
            cell.placehorderNote.isHidden = false
            }else
            {
            cell.placehorderNote.isHidden = true
            }
//            cell.noteTextView.inputAccessoryView = toolBarForMarca
            cell.noteTextView.delegate = self
        case "cellulare":
            cell.cellulareTextField.delegate = self
            cell.cellulareTextField.tag = indexPath.row
            cell.cellulareTextField.addTarget(self, action: #selector(textFiledDidChange), for: .editingChanged)
            
            cell.emailTextField.delegate = self
            cell.emailTextField.tag = 20
            cell.emailTextField.addTarget(self, action: #selector(textFiledDidChange), for: .editingChanged)
            cell.setupCell(data: dataForCell!, identifier: cell.reuseIdentifier!)
        case "nome":
            cell.nomeTextField.delegate = self
            cell.nomeTextField.tag = indexPath.row
            cell.nomeTextField.addTarget(self, action: #selector(textFiledDidChange), for: .editingChanged)
            
            cell.cognomeTextField.delegate = self
            cell.cognomeTextField.tag = 21
            cell.cognomeTextField.addTarget(self, action: #selector(textFiledDidChange), for: .editingChanged)
            cell.setupCell(data: dataForCell!, identifier: cell.reuseIdentifier!)
        case "documento":
            for i in self.indiceFoto
            {
                cell.setImage(value: i)
            }
            switch data["servizio"]! {
            case "Carrozzeria":
                cell.imageInfo.isHidden = false
                cell.imageInfo.text = infoCarrozzeria
            case "Meccanica":
                cell.imageInfo.isHidden = false
                cell.imageInfo.text = infoMeccanica
            case "Gomme":
                cell.imageInfo.isHidden = false
                cell.imageInfo.text = infoGomme
            default:
                cell.imageInfo.isHidden = true
            }
        default:
            print("altro")
        }
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let identifier = self.celle[indexPath.row]
        switch identifier {
        case "marca":
            return 86 //marca
        case "modello":
            return 86 //modello
        case "km":
            if self.isExpandCell{
                return 86
            }
            return 0 //km
        case "servizio":
            return 86 //servizio
        case "note":
            return 120
        case "cellulare":
            return 86 //cell-email
        case "nome":
            return 86 //nome-cognome
        case "documento":
            return 160 //Foto
        case "invia":
            return 80 //invia
        default:
            break
        }
        return 0
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    // MARK: - PickerDelegate
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return marca.count
        case 2:
            return servizio.count
        default:
            break
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return marca[row]
        case 2:
            return servizio[row]
        default:
            break
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 0:
            let marca = self.marca[pickerView.selectedRow(inComponent: 0)]
            data["marca"] = marca
            pickerView.endEditing(false)
        case 2:
            let servizio = self.servizio[pickerView.selectedRow(inComponent: 0)]
            data["servizio"] = servizio
            pickerView.endEditing(false)
        default:
            break
        }
    }
    
    
    @IBAction func menuAction(_ sender: Any) {
        self.menuAction()
    }
    //MARK: TextField Delegate
    var kmCharatterCount = 0
    
    func textFiledDidChange(textField: UITextField){
        let colorGray = UIColor.gray
        textField.attributedPlaceholder = NSAttributedString(string: textField.placeholder!, attributes: [NSForegroundColorAttributeName : colorGray])
        var identifier:String
        if textField.tag == 20
        {
            identifier = "email"
        }else if textField.tag == 21{
            identifier = "cognome"
        }else{
            identifier = self.celle[textField.tag]
        }
        if textField.restorationIdentifier == "km"
        {
            if (textField.text?.characters.count)! > kmCharatterCount
            {
                self.data[identifier] = textField.text
                if let range = self.data[identifier]?.range(of: " Km") {
                    self.data[identifier]?.removeSubrange(range)
                }
                textField.text = ""
                textField.text = data["km"]! + " Km"
            }else{
                if (textField.text?.characters.count)! < 4
                {
                    data["km"] = ""
                    textField.text = ""
                    textField.endEditing(true)
                }else{
                    var name: String = data["km"]!
                    let endIndex = name.index(name.endIndex, offsetBy: -1)
                    let truncated = name.substring(to: endIndex)
                    data["km"] = truncated
                    textField.text = data["km"]! + " Km"
                }
            }
            kmCharatterCount = (textField.text?.characters.count)!
        }else{
            self.data[identifier] = textField.text
        }
        animatingLabel(identifier: identifier)
    }
    
    func animatingLabel( identifier:String)
    {
        var identifierCell = identifier
        if identifier == "cognome"{identifierCell = "nome"}
        if identifier == "email"{identifierCell = "cellulare"}
        if data[identifier] != ""
        {
            let cells = self.tableView.visibleCells as! Array<preventivoCell>
            for cell in cells{
                if cell.reuseIdentifier == identifierCell{
                  cell.animateLabel(cell: cell, identifier: identifier, fadeIn: true)
                }
            }
            
        }else{
            let cells = self.tableView.visibleCells as! Array<preventivoCell>
            for cell in cells{
                if cell.reuseIdentifier == identifierCell{
                    cell.animateLabel(cell: cell, identifier: identifier, fadeIn: false)
                }
            }
        }
    }
    
    
    //MARK: TextView Delegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        let index = IndexPath(item: 4, section: 0)
        let cell = preventivoTableView.cellForRow(at: index) as! preventivoCell
        cell.placehorderNote.textColor = UIColor.lightGray
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        data["note"] = textView.text
    }
  
    func textViewDidChange(_ textView: UITextView) {
        let count = textView.text.characters.count
        let index = IndexPath(item: 4, section: 0)
        let cell = preventivoTableView.cellForRow(at: index) as! preventivoCell
        if count > 0 {
            cell.placehorderNote.isHidden = true
            cell.animateLabel(cell: cell, identifier: "note", fadeIn: true)
        }else{
            cell.placehorderNote.isHidden = false
            cell.animateLabel(cell: cell, identifier: "note", fadeIn: false)
        }
    }



    func donePressedMarca(){
        if data["marca"] == ""
        {
            data["marca"] = self.marca[0]
        }
        self.preventivoTableView.reloadData()
    }
    
    func donePressedServizio(){
        if data["servizio"] == ""
        {
            data["servizio"] = self.servizio[0]
        }
        self.preventivoTableView.reloadData()
    }
    
    func donePressedNote(){
        print("")
    }
    
    //MARK: Immagini
    
    @IBAction func openPhoto(_ sender: UIButton) {
        //        self.btnEdit.setTitleColor(UIColor.white, for: .normal)
        //        self.btnEdit.isUserInteractionEnabled = true
        //
        self.selectedImage = sender.tag
        let alert = UIAlertController(title: "Scegli Immagine", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Fotocamera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Galleria Foto", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancella", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera(){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    
    func openGallery(){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        //        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        if var index = indiceFoto.index(where: {$0.indice == selectedImage}) {
            self.indiceFoto[index].foto = image
            self.indiceFoto[index].placeholder = false
        }
        //        foto.append(image)
        //        self.placeholder = false
        self.preventivoTableView.reloadData()
        dismiss(animated:true, completion: nil)
    }
    
    @IBAction func visualizza(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "fotoView") as! fotoViewController
        vc.delegate = self
        self.selectedImage = sender.tag
        if var index = indiceFoto.index(where: {$0.indice == self.selectedImage}) {
            vc.imageToShow = self.indiceFoto[index].foto
            vc.indexFoto = self.indiceFoto[index].indice
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func deleteFoto(index: Int) {
        if var index = indiceFoto.index(where: {$0.indice == selectedImage}) {
            self.indiceFoto[index].setplaceholder()
        }
        self.preventivoTableView.reloadData()
    }
    //MARK: SEND PREVENTIVO
    @IBAction func sendPreventivo(_ sender: Any) {
        
        var ok = false
        let servizio:String = data["servizio"]! as String
        if data["marca"] != "" && data["modello"] != "" && data["servizio"] != "" && data["cellulare"] != "" && data["email"] != "" && data["nome"] != "" && data["cognome"] != ""{
            
            
            
            switch servizio {
            case "Carrozzeria":
                if indiceFoto.count == 2 || (data["note"] != "")
                {
                    if indiceFoto[0].placeholder == false && indiceFoto[1].placeholder == false{
                    ok = true
                    formattaEmail(servizio: servizio)
                    }
                }
            case "Meccanica":
                if indiceFoto.count > 0 || (data["note"] != "") || (data["km"] != "")
                {
                    if indiceFoto[0].placeholder == false && indiceFoto[1].placeholder == false
                    {
                    ok = true
                    formattaEmail(servizio: servizio)
                    }
                }
            default:
                if indiceFoto.count > 0
                {
                    if indiceFoto[0].placeholder == false && indiceFoto[1].placeholder == false{
                    ok = true
                    formattaEmail(servizio: servizio)
                    }
                }
            }
            
        }
        if !ok{
            let alert = UIAlertView(title: "Attenzione", message: "Non tutti i campi obbligatori sono stati riempiti", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            let cells = self.tableView.visibleCells as! Array<preventivoCell>
            
            for cell in cells {
                cell.checkTextfield(cell: cell, servizio: servizio)
            }

        }
    }
    var finalString: String?
    func formattaEmail(servizio: String)
    {
        switch servizio {
        case "Carrozeria":
            self.finalString = "Salve,\n desiderei ricevere un preventivo per un intervento di CARROZZERIA \nmarca: \(data["marca"]!)\nmodello:\(data["modello"]!)\nla quale deve subire un intervento di \(data["servizio"]!).\n In attesa di una risposta lascio qui di seguito i miei recapiti.\n Nome:\(data["nome"]!)\nCognome:\(data["cognome"]!)\nNumero di telefono: \(data["cellulare"]!)\nEmail:\(data["email"]!)\n\nP.s. \(data["note"]!))"
        case "Meccanica":
            self.finalString = "Salve,\n desiderei ricevere un preventivo per un intervento MECCANICO \nmarca: \(data["marca"]!)\nmodello:\(data["modello"]!)\nla quale deve subire un intervento di \(data["servizio"]!).\n In attesa di una risposta lascio qui di seguito i miei recapiti.\n Nome:\(data["nome"]!)\nCognome:\(data["cognome"]!)\nNumero di telefono: \(data["cellulare"]!)\nEmail:\(data["email"]!)\n\nP.s. \(data["note"]!))"
        default:
            self.finalString = "Salve,\n desiderei ricevere un preventivo per un cambio GOMME \nmarca: \(data["marca"]!)\nmodello:\(data["modello"]!)\nla quale deve subire un intervento di \(data["servizio"]!).\n In attesa di una risposta lascio qui di seguito i miei recapiti.\n Nome:\(data["nome"]!)\nCognome:\(data["cognome"]!)\nNumero di telefono: \(data["cellulare"]!)\nEmail:\(data["email"]!)\n\nP.s. \(data["note"]!))"
        }
        if( MFMailComposeViewController.canSendMail() ) {
            print("Can send email.")
            
            let mailComposer = configureMailComposer()
            
            //Set the subject and message of the email
            
            self.present(mailComposer, animated: true, completion: nil)
        }
    }
    
    func configureMailComposer()->MFMailComposeViewController
    {
        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self
        mailComposer.setToRecipients(["paolo.berluti@gmail.com"])
        mailComposer.setSubject("Preventivo \(data["nome"]!) \(data["cognome"]!)")
        mailComposer.setMessageBody(finalString!, isHTML: false)
        for image in indiceFoto
        {
            if !image.placeholder{
                let imageData: NSData = (UIImageJPEGRepresentation(image.foto!, 0.5) as? NSData)!
                mailComposer.addAttachmentData(imageData as Data, mimeType: "image/jpeg", fileName: "image.jpg")
            }
        }
        return mailComposer
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
// Estensione per creare il Toolbar
extension UIToolbar {
    
    func ToolbarPiker(mySelect : Selector) -> UIToolbar {
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Ok", style: UIBarButtonItemStyle.plain, target: self, action: mySelect)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([ spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
}

