//
//  MenuViewController.swift
//  autosalone
//
//  Created by Paolo Berluti on 10/07/17.
//  Copyright © 2017 Giovanni Giometti. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    @IBOutlet weak var leadingMenu: NSLayoutConstraint!
    @IBOutlet weak var viewMenu: UIView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    @IBOutlet weak var preventivoBtn: UIButton!
    
    //INDICATORI
    @IBOutlet weak var homeInd: UIView!
    @IBOutlet weak var interventoInd: UIView!
    @IBOutlet weak var preventivoInd: UIView!
    @IBOutlet weak var contattiInd: UIView!
    //IMMAGINI DEL MENU
    @IBOutlet weak var home: UIImageView!
    @IBOutlet weak var intervento: UIImageView!
    @IBOutlet weak var preventivo: UIImageView!
    @IBOutlet weak var contatti: UIImageView!
    //BOTTONI
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var interventoBtn: UIButton!
    @IBOutlet weak var preventiviBtn: UIButton!
    @IBOutlet weak var contattiBtn: UIButton!
    
    
    var immagini = ["home","intervento","preventivo","contact"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewMenu.layer.shadowOpacity = 0
        self.blurView.layer.shadowOpacity = 1
        self.viewMenu.layer.shadowRadius = 40
        self.blurView.layer.shadowRadius = 40
        
        preventivoBtn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        preventivoBtn.contentHorizontalAlignment = .left
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.35, animations: {
            self.viewMenu.frame.origin.x += 300
            self.blurView.frame.origin.x += 300
        }, completion: nil)
        let mode = AppDelegate.storeViewController
        let colorSelect = UIColor(red: 30/255, green: 39/255, blue: 55/255, alpha: 1.0) /* #1e2737 */
        let normalColor = UIColor(red: 87/255, green: 101/255, blue: 122/255, alpha: 1.0) /* #57657a */
        switch mode {
        case "Home":
            self.homeInd.isHidden = false
            self.interventoInd.isHidden = true
            self.preventivoInd.isHidden = true
            self.contattiInd.isHidden = true
            
            self.home.image = #imageLiteral(resourceName: "home_old")
            self.intervento.image = #imageLiteral(resourceName: "intervento")
            self.preventivo.image = #imageLiteral(resourceName: "preventivo")
            self.contatti.image = #imageLiteral(resourceName: "contact")
        case "Intervento":
            self.homeInd.isHidden = true
            self.interventoInd.isHidden = false
            self.preventivoInd.isHidden = true
            self.contattiInd.isHidden = true
            
            self.home.image = #imageLiteral(resourceName: "home")
            self.intervento.image = #imageLiteral(resourceName: "intervento_old")
            self.preventivo.image = #imageLiteral(resourceName: "preventivo")
            self.contatti.image = #imageLiteral(resourceName: "contact")
        case "Preventivo":
            self.homeInd.isHidden = true
            self.interventoInd.isHidden = true
            self.preventivoInd.isHidden = false
            self.contattiInd.isHidden = true
            
            self.home.image = #imageLiteral(resourceName: "home")
            self.intervento.image = #imageLiteral(resourceName: "intervento")
            self.preventivo.image = #imageLiteral(resourceName: "preventivo_old")
            self.contatti.image = #imageLiteral(resourceName: "contact")
        default:
            self.homeInd.isHidden = true
            self.interventoInd.isHidden = true
            self.preventivoInd.isHidden = true
            self.contattiInd.isHidden = false
            
            self.home.image = #imageLiteral(resourceName: "home")
            self.intervento.image = #imageLiteral(resourceName: "intervento")
            self.preventivo.image = #imageLiteral(resourceName: "preventivo")
            self.contatti.image = #imageLiteral(resourceName: "contact_old")
        }
    }
 
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    func animationOut(completionHandler:@escaping (Bool) -> ()) {
        UIView.animate(withDuration: 0.35, animations: {
            self.viewMenu.frame.origin.x -= 300
            self.blurView.frame.origin.x -= 300
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0)
        }, completion: { (finished: Bool) in
        completionHandler(true)
        })
    }
    
    @IBAction func navigationMenuBtn(_ sender: UIButton) {
        var controller: UINavigationController?
        switch sender.tag {
        case 0:
            if let viewControllers = navigationController?.viewControllers {
                for viewController in viewControllers {
                    // some process
                    if viewController is HomeViewController{
                    }else{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        controller = storyboard.instantiateViewController(withIdentifier: "homeViewController") as? UINavigationController
                        AppDelegate.storeViewController = "Home"
                        self.presentDetail(controller!)
                    }
                }
            }
        case 1:
            if let viewControllers = navigationController?.viewControllers {
                for viewController in viewControllers {
                    // some process
                    if viewController is InterventoTableViewController{
                    }else{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        controller = storyboard.instantiateViewController(withIdentifier: "interventoViewController") as? UINavigationController
                        AppDelegate.storeViewController = "Intervento"
                        self.presentTableDetail(controller!)
                    }
                }
            }
        case 2:
            if let viewControllers = navigationController?.viewControllers {
                for viewController in viewControllers {
                    // some process
                    if viewController is PreventivoTableViewController{
                    }else{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        controller = storyboard.instantiateViewController(withIdentifier: "preventiviViewController") as? UINavigationController
                        AppDelegate.storeViewController = "Preventivo"
                        self.presentDetail(controller!)
                    }
                }
            }
        case 3:
            if let viewControllers = navigationController?.viewControllers {
                for viewController in viewControllers {
                    // some process
                    if viewController is ContattiViewController{
                    }else{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        controller = storyboard.instantiateViewController(withIdentifier: "contattiViewController") as? UINavigationController
                        AppDelegate.storeViewController = "Contatti"
                        self.presentDetail(controller!)
                    }
                }
            }
        default:
            break
        }
        AppDelegate.menu_bool = true
    }
    @IBAction func closeMenu(_ sender: Any) {
        self.animationOut() { success in
            self.view.removeFromSuperview()
            AppDelegate.menu_bool = true
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
