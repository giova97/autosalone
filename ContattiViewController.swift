//
//  ContattiViewController.swift
//  autosalone
//
//  Created by Giovanni Giometti on 17/07/17.
//  Copyright © 2017 Giovanni Giometti. All rights reserved.
//

import UIKit
import MapKit

class ContattiViewController: BaseViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var myMapView: MKMapView! //dichiarazione outlet mappa
    var arrayMap = ["waze://", "comgooglemaps://"]
    var arrayAlert = ["Waze", "Google Maps"]
    let location = CLLocationCoordinate2D(latitude: 41.937480, longitude: 12.424726)
    var managerPosizione: CLLocationManager! //gestioe oggetto LocationManager
    var posizioneUtente: CLLocationCoordinate2D! //conserva le tue coordinate (LocationCoordinate)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.myMapView.delegate = self  //delegate per la gestione della myMapView
        self.managerPosizione = CLLocationManager()  //variabile utiliazzata per contenere la posizione
        managerPosizione.delegate = self
        managerPosizione.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        managerPosizione.requestWhenInUseAuthorization()
        managerPosizione.startUpdatingLocation()
       
        
        
        // Do any additional setup after loading the view.
    }
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0){
            self.locationAutosalone()
        }
    }
    func locationAutosalone() {
        let pointAutosalone = MKPointAnnotation()
        
        let span = MKCoordinateSpanMake(0.0009, 0.0009)
        pointAutosalone.title = "Autosalone"
        pointAutosalone.coordinate = location
        let region = MKCoordinateRegionMake(location, span)
        //nuovoRicordo.canShowCallout = true
        
        
        self.myMapView.addAnnotation(pointAutosalone)
        self.myMapView.setRegion(region, animated: true)
        //print("PointAnnotation creato")
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        pinView.canShowCallout = true
        if pinView.annotation?.coordinate.latitude != nil{
            //            let button = UIButton(type: .infoDark)
            let button = UIButton()
            button.setImage(#imageLiteral(resourceName: "navigate"), for: .normal)
            button.frame.size.height = 30
            button.frame.size.width = 30
            button.addTarget(self, action: #selector(externalMap), for: .touchUpInside)
            pinView.rightCalloutAccessoryView = button
        }
        return pinView
    }
    
    func externalMap(){
        canOpen()
        let alert = UIAlertController(title: "Seleziona", message: "Seleziona il navigatore", preferredStyle: .actionSheet)
        for i in 0...self.arrayMap.count - 1 {
            let app = self.arrayMap[i]
            if app != ""
            {
                let button = UIAlertAction(title: self.arrayAlert[i], style: .default, handler: { action in
                    let url = URL(string: app)
                    UIApplication.shared.open(url!, options: [:], completionHandler: nil)
//                    AppDelegate.storeViewController = "Contatti"
                })
                alert.addAction(button)
            }
        }
        self.present(alert, animated: true, completion:{
            self.addDismissControl(alert.view)
        })
    }
    
    
    func addDismissControl(_ toView: UIView) {
        let dismissControl = UIControl()
        dismissControl.addTarget(self, action: #selector(self.dismissAlertViewController), for: .touchDown)
        dismissControl.frame = toView.superview?.frame ?? CGRect.zero
        toView.superview?.insertSubview(dismissControl, belowSubview: toView)
    }
    
    func dismissAlertViewController()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func canOpen()
    {
        for i in 0...arrayMap.count - 1{
            let url = URL(string: arrayMap[i])
            if UIApplication.shared.canOpenURL(url!)
            {
                let app = arrayMap[i]
                if app.contains("waze"){
                    //                    arrayMap[i] = "waze://?ll=\(location.latitude),\(location.longitude)&navigate=yes"
                    
                    arrayMap[i] = "https://waze.com/ul?ll=\(location.latitude),\(location.longitude)&navigate=no"
                }else if app.contains("google"){
                    arrayMap[i] = "http://maps.google.com/?daddr=\(location.latitude),\(location.longitude)&directionsmode=driving"
                }
            }else
            {
                //                arrayMap[i] = ""
                arrayAlert[i] = ""
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        AppDelegate.storeViewController = "Home"
    }
    @IBAction func goMenu(_ sender: Any) {
        self.menuAction()
    }
    
    @IBAction func callMe(_ sender: UIButton) {
        if sender.tag == 0
        {
            guard let number = URL(string: "tel://" + "063380307") else { return }
            UIApplication.shared.open(number)
        }else{
            guard let number = URL(string: "tel://" + "0666416351") else { return }
            UIApplication.shared.open(number)
        }
    }
    
    @IBAction func navigMe(_ sender: UIButton) {
        if sender.tag == 0
        {
            externalMap()
        }else{
            externalMap()
        }
    }
   
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
