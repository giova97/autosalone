//
//  preventivoCell.swift
//  autosalone
//
//  Created by Giovanni Giometti on 18/07/17.
//  Copyright © 2017 Giovanni Giometti. All rights reserved.
//

import UIKit

class preventivoCell: UITableViewCell {
    
    @IBOutlet weak var marcaTextField: UITextField!
    
    @IBOutlet weak var modelloTextField: UITextField!
    
    @IBOutlet weak var kmTextField: UITextField!
    
    @IBOutlet weak var servizioTextField: UITextField!
    
    @IBOutlet weak var noteTextField: UITextField!
    
    @IBOutlet weak var cellulareTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var nomeTextField: UITextField!
    
    @IBOutlet weak var noteTextView: UITextView!
    
    @IBOutlet weak var cognomeTextField: UITextField!
    
    @IBOutlet weak var aggDocumentoButton: UIButton!
    
    @IBOutlet weak var inviaButton: UIButton!
    
    @IBOutlet weak var img1: UIImageView!
    
    @IBOutlet weak var visualizzaImg1: UIButton!
    
    @IBOutlet weak var btnImg1: UIButton!
    
    @IBOutlet weak var img2: UIImageView!
    
    @IBOutlet weak var visualizzaImg2: UIButton!
    
    @IBOutlet weak var btnImg2: UIButton!
    
    @IBOutlet weak var placehorderNote: UILabel!
    
    @IBOutlet weak var imageInfo: UILabel!
    
    //ETICHETTE
    @IBOutlet weak var marcaEtichetta: UILabel!
    @IBOutlet weak var modelloEtichetta: UILabel!
    @IBOutlet weak var servizioEtichetta: UILabel!
    @IBOutlet weak var kmEtichetta: UILabel!
    @IBOutlet weak var noteEtichetta: UILabel!
    @IBOutlet weak var cellulareEtichetta: UILabel!
    @IBOutlet weak var emailEtichetta: UILabel!
    @IBOutlet weak var nomeEtichetta: UILabel!
    @IBOutlet weak var cognomeEtichetta: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupCell(data: String, identifier: String){
        switch identifier {
        case "marca":
            if check(value: data){
                self.marcaTextField.text = data
            }
        case "modello":
            self.modelloTextField.addDoneButtonOnKeyboard()
        case "servizio":
            if check(value: data){
                self.servizioTextField.text = data
            }
        case "km":
            self.kmTextField.addDoneButtonOnKeyboard()
        case "note":
            self.noteTextView.addDoneButtonOnKeyboard()
        case "cellulare":
            self.cellulareTextField.addDoneButtonOnKeyboard()
            self.emailTextField.addDoneButtonOnKeyboard()
        case "nome":
            self.nomeTextField.addDoneButtonOnKeyboard()
            self.cognomeTextField.addDoneButtonOnKeyboard()
        default:
            break
        }
    }
    func check(value: String) -> Bool
    {
        if value != ""
        {
            return true
        }
        return false
    }
    func setImage(value: IndiceFoto)
    {
        if value.placeholder
        {
            switch value.indice {
            case 1:
                self.img1.image = value.foto
                self.btnImg1.alpha = 1
                self.visualizzaImg1.alpha = 0
            case 2:
                self.img2.image = value.foto
                self.btnImg2.alpha = 1
                self.visualizzaImg2.alpha = 0
            default:
                self.img1.image = value.foto
            }
        }else{
            switch value.indice {
            case 1:
                self.img1.image = value.foto
                self.btnImg1.alpha = 0
                self.visualizzaImg1.alpha = 1
            case 2:
                self.img2.image = value.foto
                self.btnImg2.alpha = 0
                self.visualizzaImg2.alpha = 1
            default:
                self.img1.image = value.foto
            }
        }
    }
    func checkTextfield(cell: preventivoCell, servizio: String){
        let colorRed = UIColor.red
        for child in cell.contentView.subviews {
            
            if let textfield = child as? UITextField {
                if textfield.text == ""
                {
                    if textfield.restorationIdentifier == "note"{
                        
                    } else {
                        textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder!, attributes: [NSForegroundColorAttributeName : colorRed])
                    }
                }
            } else if let label = child as? UILabel
            {
                if label.restorationIdentifier == "placeNote"{
                    if (servizio == "Carrozzeria" || servizio == "Meccanica"){
                        label.textColor = colorRed
                    }else{
                        label.textColor = UIColor.lightGray
                    }
                }
            }
        }
    }
    func animateLabel(cell: preventivoCell, identifier: String, fadeIn:Bool)
    {
        var identifier = identifier + "Etc"
        for child in cell.contentView.subviews {
            if let label = child as? UILabel {
                if label.restorationIdentifier == identifier{
                self.animate(label: label, fadeIn: fadeIn)
                }
            }
        }
    }
//MARK: ANIMATION
    func animate(label: UILabel, fadeIn:Bool) {
        var origin = label.center.y
        if fadeIn{
            if label.alpha == 0 {
                label.center.y = origin + 20
                UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: ({
                    label.center.y = origin//self.view.frame.height - 20//0.3 /// 1.008
                    label.alpha = 1
                }), completion: nil)
            }
        } else {
            if label.alpha == 1 {
                //                label.center.y = self.view.frame.height + 30
                UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: ({
                    label.center.y = origin + 20//label.center.y + 20//self.view.frame.height + 0.3 /// 1.008
                    label.alpha = 0
                }), completion: { (finished) -> Void in
                    label.center.y = origin})
            }
        }
    }

}
extension UITextField{
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}
extension UITextView{
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}

